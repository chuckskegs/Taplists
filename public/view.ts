
let menu: string; // name of the store
const menuHeader = {
    tap: "Tap",
    beer: "Brewery: Beer Name",
    Full: "Full",
    Half: "8 oz",
    Quarter: "4 oz",
    Crowler: "Crowler",
    Growler: "Growler",
    origin: "Origin",
    abv: "%ABV",
}

/** 
 * Initial function anytime someone opens webpage: If taplist.web.app 
 * is visited with paramter ?store={storeID} then it will load up with 
 * the menu of the given storoID 
 */
const initiateTable = () => {
    const initial = getUrlParameter(`store`)
    if (initial && initial === `CD`) {
        menu = `CD`;
        document.title = `Chuck's CD Taplist`
        loadDisplay();
    } else if (initial && initial ===  `GW`) {
        menu = `GW`;
        document.title = `Chuck's GW Taplist`
        loadDisplay();
    } else if (initial && initial ===  `SP`) {
        menu = `SP`;
        document.title = `Chuck's SP Taplist`;
        loadDisplay()
    }
    window.setInterval(updateSquare, 120000);
}

var getUrlParameter = function getUrlParameter(sParam: any) {
    var sPageURL = window.location.search.substring(1),
        sURLVariables = sPageURL.split('&'),
        sParameterName,
        i;
    for (i = 0; i < sURLVariables.length; i++) {
        sParameterName = sURLVariables[i].split('=');
        if (sParameterName[0] === sParam) {
            return sParameterName[1] === undefined ? true : decodeURIComponent(sParameterName[1]);
        }
    }
};

// Sets the menu variable to the proper locationID 
const setMenu = (req: MouseEvent) => {
    //@ts-ignore
    menu = req.target.id;
    loadDisplay();
}

const updateSquare = () => {
    $.get('/updateSquare');
    $("#lastRefresh").text(`Last Refresh: ${new Date().toLocaleTimeString()}`);
}

// Loads the proper menu of the given store
const loadDisplay = () => { 
    let title = document.getElementById('StoreTitle');
    let color = '#1134a6';
    if(menu.startsWith("CD")){
        title!.textContent = "Central District";
    }else if(menu.startsWith("SP")){
        color = '#FFA500';
        title!.textContent = "Seward Park";
    }else if(menu.startsWith("GW")){
        color = '#11a618';
        title!.textContent = "Greenwood";
    }
    let query = { menu: menu };
    // Http request to express app in Controller.ts
    $.get(`/data`, query).then(generateTable);
    $("#headers").css({"background-color": color});
};

// Rewrites table data
const generateTable = (data: object[]) => {
    if (typeof(data) === "string") {
        alert(data);
    }
    var tBody = document.getElementById("table-body")!; 
    tBody.innerHTML = "";
    data.map(obj => {
        let row = document.createElement("tr");
        tBody.appendChild(row);
        // @ts-ignore
        if (obj.beer.startsWith("-") || obj.beer.startsWith("_")) {
            return console.log("Not Displayed: ", obj); 
        }
        Object.keys(menuHeader).map(key => {
            let eventDetail = (obj as any)[key];
            let cell = document.createElement("td"); 
            // @ts-ignore
            cell.className = `${key} row${row.rowIndex} ${obj.type || "Pilsner"}`;
            // @ts-ignore
            if (obj.serving != "16 oz") {
                cell.className += " differentSize";
            }
            if (eventDetail === 0 || eventDetail === "0") {
                eventDetail = "";
            }
            cell.textContent = eventDetail;
            row.appendChild(cell);
        });
    });
}

document.addEventListener('DOMContentLoaded', initiateTable);
document.getElementById('CD1')?.addEventListener('click', setMenu);
document.getElementById('CD2')?.addEventListener('click', setMenu);
document.getElementById('CD')?.addEventListener('click', setMenu);
document.getElementById('GW1')?.addEventListener('click', setMenu);
document.getElementById('GW2')?.addEventListener('click', setMenu);
document.getElementById('GW')?.addEventListener('click', setMenu);
document.getElementById('SP1')?.addEventListener('click', setMenu);
document.getElementById('SP2')?.addEventListener('click', setMenu);
document.getElementById('SP')?.addEventListener('click', setMenu);

//Refresh updates the POS system for the store that the page is currently on
document.getElementById('Refresh')?.addEventListener('click', ()=> { 
    updateSquare();
    loadDisplay;
});