"use strict";
var _a, _b, _c, _d, _e, _f, _g, _h, _j, _k;
let menu; // name of the store
const menuHeader = {
    tap: "Tap",
    beer: "Brewery: Beer Name",
    Full: "Full",
    Half: "8 oz",
    Quarter: "4 oz",
    Crowler: "Crowler",
    Growler: "Growler",
    origin: "Origin",
    abv: "%ABV",
};
/**
 * Initial function anytime someone opens webpage: If taplist.web.app
 * is visited with paramter ?store={storeID} then it will load up with
 * the menu of the given storoID
 */
const initiateTable = () => {
    const initial = getUrlParameter(`store`);
    if (initial && initial === `CD`) {
        menu = `CD`;
        document.title = `Chuck's CD Taplist`;
        loadDisplay();
    }
    else if (initial && initial === `GW`) {
        menu = `GW`;
        document.title = `Chuck's GW Taplist`;
        loadDisplay();
    }
    else if (initial && initial === `SP`) {
        menu = `SP`;
        document.title = `Chuck's SP Taplist`;
        loadDisplay();
    }
    window.setInterval(updateSquare, 120000);
};
var getUrlParameter = function getUrlParameter(sParam) {
    var sPageURL = window.location.search.substring(1), sURLVariables = sPageURL.split('&'), sParameterName, i;
    for (i = 0; i < sURLVariables.length; i++) {
        sParameterName = sURLVariables[i].split('=');
        if (sParameterName[0] === sParam) {
            return sParameterName[1] === undefined ? true : decodeURIComponent(sParameterName[1]);
        }
    }
};
// Sets the menu variable to the proper locationID 
const setMenu = (req) => {
    //@ts-ignore
    menu = req.target.id;
    loadDisplay();
};
const updateSquare = () => {
    $.get('/updateSquare');
    $("#lastRefresh").text(`Last Refresh: ${new Date().toLocaleTimeString()}`);
};
// Loads the proper menu of the given store
const loadDisplay = () => {
    let title = document.getElementById('StoreTitle');
    let color = '#1134a6';
    if (menu.startsWith("CD")) {
        title.textContent = "Central District";
    }
    else if (menu.startsWith("SP")) {
        color = '#FFA500';
        title.textContent = "Seward Park";
    }
    else if (menu.startsWith("GW")) {
        color = '#11a618';
        title.textContent = "Greenwood";
    }
    let query = { menu: menu };
    // Http request to express app in Controller.ts
    $.get(`/data`, query).then(generateTable);
    $("#headers").css({ "background-color": color });
};
// Rewrites table data
const generateTable = (data) => {
    if (typeof (data) === "string") {
        alert(data);
    }
    var tBody = document.getElementById("table-body");
    tBody.innerHTML = "";
    data.map(obj => {
        let row = document.createElement("tr");
        tBody.appendChild(row);
        // @ts-ignore
        if (obj.beer.startsWith("-") || obj.beer.startsWith("_")) {
            return console.log("Not Displayed: ", obj);
        }
        Object.keys(menuHeader).map(key => {
            let eventDetail = obj[key];
            let cell = document.createElement("td");
            // @ts-ignore
            cell.className = `${key} row${row.rowIndex} ${obj.type || "Pilsner"}`;
            // @ts-ignore
            if (obj.serving != "16 oz") {
                cell.className += " differentSize";
            }
            if (eventDetail === 0 || eventDetail === "0") {
                eventDetail = "";
            }
            cell.textContent = eventDetail;
            row.appendChild(cell);
        });
    });
};
document.addEventListener('DOMContentLoaded', initiateTable);
(_a = document.getElementById('CD1')) === null || _a === void 0 ? void 0 : _a.addEventListener('click', setMenu);
(_b = document.getElementById('CD2')) === null || _b === void 0 ? void 0 : _b.addEventListener('click', setMenu);
(_c = document.getElementById('CD')) === null || _c === void 0 ? void 0 : _c.addEventListener('click', setMenu);
(_d = document.getElementById('GW1')) === null || _d === void 0 ? void 0 : _d.addEventListener('click', setMenu);
(_e = document.getElementById('GW2')) === null || _e === void 0 ? void 0 : _e.addEventListener('click', setMenu);
(_f = document.getElementById('GW')) === null || _f === void 0 ? void 0 : _f.addEventListener('click', setMenu);
(_g = document.getElementById('SP1')) === null || _g === void 0 ? void 0 : _g.addEventListener('click', setMenu);
(_h = document.getElementById('SP2')) === null || _h === void 0 ? void 0 : _h.addEventListener('click', setMenu);
(_j = document.getElementById('SP')) === null || _j === void 0 ? void 0 : _j.addEventListener('click', setMenu);
//Refresh updates the POS system for the store that the page is currently on
(_k = document.getElementById('Refresh')) === null || _k === void 0 ? void 0 : _k.addEventListener('click', () => {
    updateSquare();
    loadDisplay;
});
