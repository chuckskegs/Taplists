import { Shop, CD, GW, SP, key, token, menuHeader } from "./variables";
import http, { AxiosResponse, AxiosError } from 'axios';
import firebase from "firebase/app";
// eslint-disable-next-line import/no-unassigned-import
import 'firebase/database';

const config = {
    apiKey: "AIzaSyCNY9wMC8RJ2E-F655k91c5Fl5RyilIUfQ",
    authDomain: "taplists.firebaseapp.com",
    databaseURL: "https://taplists.firebaseio.com",
    projectId: "taplists",
    storageBucket: "taplists.appspot.com",
    messagingSenderId: "968158600113",
    appId: "1:968158600113:web:1688b87dc85b226f4bfb90",
    measurementId: "G-CCM3SSY2VS"
  };
  
firebase.initializeApp(config);
const database = firebase.database();
const rootref = database.ref();

let addFullSP: number, addHalfSP: number, addQuarterSP: number, multiplierSP : number, minHalfSP: number, minQuarterSP: number, minPriceSP: number;
let addFullCD: number, addHalfCD: number, addQuarterCD: number, multiplierCD : number, minHalfCD: number, minQuarterCD: number, minPriceCD: number;
let addFullGW: number, addHalfGW: number, addQuarterGW: number, multiplierGW : number, minHalfGW: number, minQuarterGW: number, minPriceGW: number;

async function getVariables(){
    const spRef = rootref.child('taplistPricingSP');
    await spRef.once('value').then((snapshot: any) => {
        let data = snapshot.val();
        addFullSP = data.addFull;
        addHalfSP = data.addHalf;
        addQuarterSP = data.addQuarter;
        multiplierSP = data.multiplier;
        minHalfSP = data.minHalf;
        minQuarterSP = data.minQuarter;
        minPriceSP = data.minFull;
    }).catch((error: any) => {
        console.error(error);
    });
    const cdRef = rootref.child('taplistPricingCD');
    await cdRef.once('value').then((snapshot: any) => {
        let data = snapshot.val();
        addFullCD = data.addFull;
        addHalfCD = data.addHalf;
        addQuarterCD = data.addQuarter;
        multiplierCD = data.multiplier;
        minHalfCD = data.minHalf;
        minQuarterCD = data.minQuarter;
        minPriceCD = data.minFull;
    }).catch((error: any) => {
        console.error(error);
    });
    const gwRef = rootref.child('taplistPricingGW');
    await gwRef.once('value').then((snapshot: any) => {
        let data = snapshot.val();
        addFullGW = data.addFull;
        addHalfGW = data.addHalf;
        addQuarterGW = data.addQuarter;
        multiplierGW = data.multiplier;
        minHalfGW = data.minHalf;
        minQuarterGW = data.minQuarter;
        minPriceGW = data.minFull;
    }).catch((error: any) => {
        console.error(error);
    });
}

/**
 * Returns current taplist data based on location
 * API connection to trello to recieve card data and create beer objects to be 
 * added to the Menu & POS system.
 */
async function getData (menu: string) {
    let shop = menu.startsWith('GW') ? GW : (menu.startsWith('CD') ? CD : SP);
    let list = shop.list;
    let start, end;
    if(menu.charAt(2) === '1'){
        end = 25;
    }else if(menu.charAt(2) === '2'){
        start = 25;
    }
    let getUrl = (listId: string) => `https://api.trello.com/1/lists/${listId}/cards/?customFieldItems=true&key=${key}&token=${token}`;
    
    try{
        // Use axios to synchronously get JSON from apropriate list using async/await
        let jsonDeck: Array<JSON> = await http.get(getUrl(list))
            .then((res: AxiosResponse) => res.data)
            .catch((err: AxiosError) => console.log(`My Deck Error: ${err.message}`));    

        // Synchronously retrieves custom definition data from Trello
        let url = () => `https://api.trello.com/1/boards/${shop.board}/customFields?key=${key}&token=${token}`;
        let customDef = await http.get(url())
            .then((res: AxiosResponse) => getCustomDefinition(res.data))
            .catch((err: AxiosError) => console.log(`My Customs Error: ${err.message}`));

        await getVariables();

        // Looks at each card from Trello and creates Beer objects using the definitions made
        let cards = jsonDeck.map((card: any, index: number) => new (Beer as any)(card, customDef, shop, index));
        cards = cards.slice(start, end);
        return cards;
    }catch (error){
        console.log(`Error in getData: ${error}`);
        throw error;
    }
}

/**
  * Make Beer objects from Trello JSON information.
  * @param {object} card The json object representing Trello card
  * @param {object} customDef The definitions object created
  * @param {number} index [Optional] The current index in array
  */ 
 const Beer = function (this: any, card: any, customDef: any, shop: Shop, index: number) {
    // Checks if index exists (first expression) and then considers the second statement
    !isNaN(index) && (this.tap = (index + 1));
    if (!card.name) {console.log("no name :", card)}
    this.beer = card.name;
    if (!shop.name) {console.log("no shop :", card)}
    this.shop = shop.name;

    let cardCustoms = card.customFieldItems;    
    cardCustoms.map((customInfo: any) => {
        if (!customInfo) { return "Error making customs"}
        let fieldName = customDef[customInfo.idCustomField].name;
        
        // If there is a Value in menuHeader with same name, apply the name of the Key instead (ex. "%ABV" => "abv")
        const found = Object.keys(menuHeader).find((title: string) => (menuHeader as any)[title] === fieldName);
        if (found) {fieldName = found};
        if (customInfo.value) {
            this[fieldName] = customInfo.value[Object.keys(customInfo.value)[0]];
        } else if (fieldName === "Color") { 
            this.type = customDef[customInfo.idCustomField][customInfo.idValue].value;
            this.color = customDef[customInfo.idCustomField][customInfo.idValue].color;
        } else {
            this[fieldName] = customDef[customInfo.idCustomField][customInfo.idValue].value; 
        }
        return customInfo;
    });

    // Default to 16oz serving size
    if (!this.serving) {
        this.serving = "16 oz";
    }
    let addHalf = addHalfSP;
    let minHalf = minHalfSP;
    let addQuarter = addQuarterSP;
    let minQuarter = minQuarterSP;
    if(this.shop === "CD"){
        addHalf = addHalfCD;
        minHalf = minHalfCD;
        addQuarter = addQuarterCD;
        minQuarter = minQuarterCD;
    }else if(this.shop === "GW"){
        addHalf = addHalfGW;
        minHalf = minHalfGW;
        addQuarter = addQuarterGW;
        minQuarter = minQuarterGW;
    }
    // Calculates price based on keg size, keg cost, and serving size
    this.Full = calculatePrice(this);
    this.Half = (Math.round((this.Full / 2 + addHalf) * 2) / 2);
    this.Half = Math.max(minHalf, this.Half).toFixed(2);
    this.Quarter = (Math.round((this.Full / 4 + addQuarter) * 4) / 4).toFixed(2);
    this.Quarter = Math.max(minQuarter, this.Quarter).toFixed(2);
    this.Growler = Math.round(this.Full * 3);
    this.Crowler = (Math.round((this.Growler / 2 + 1) * 2) / 2).toFixed(1);
    if (this.NoGr) {
        this.Growler = 0;
        this.Crowler = 0; 
    }
    if(this.No8){
        this.Half = 0;
    }
    if(this.No4){
        this.Quarter = 0;
    }
    if(this.No16){
        this.Full = 0;
    }
}

/**
 * Uses Beer object properties to determine price calculation
 * @param { Object } Beer Object with beer data
 * @property { Object } Size required on Beer object
 * @returns { number } Price
 */
const calculatePrice = (beer: any) => {
    if (beer[`$Override`]) { return beer[`$Override`] };
    let price = 0; 
    // Determine Oz's in keg based on keg size
    switch (beer["Size"]) { 
        case `1/6, 20L, 5.16G`:
            beer.oz = 660;
            break;
        case `1/4, 30L, 7.75G`:
            beer.oz = 980;
            break;
        case `50L, 13.2G`:
            beer.oz = 1690;
            break;
        case `1/2, 58L, 15.5G`:
            beer.oz = 1984;
            break;
    }
    let multiplier = multiplierSP;
    let addFull = addFullSP;
    let minPrice = minPriceSP;
    if(beer.shop === "GW"){
        multiplier = multiplierGW;
        addFull = addFullGW;
        minPrice = minPriceGW;
    }else if(beer.shop === "CD"){
        multiplier = multiplierCD;
        addFull = addFullCD;
        minPrice = minPriceCD;
    }
    // Set cost based on keg price and ounces received
    beer.costOz = beer["Keg$"] / beer.oz;
    beer.priceOz = beer.costOz * multiplier;
    price = beer.priceOz * 16 + addFull;
    price = Math.max(price, minPrice);
    // Uses "Special" field from Trello to make adjustments to price
    if (beer.Special === "Nitro") {
        price = beer.priceOz * 16 + addFull;
        price = Math.max(price, minPrice);
    }
    if (beer.abv && beer.abv >= 0) { 
        beer.abv = (Math.round(beer.abv * 10) / 10).toFixed(1);
    };
    // Round up [to nearest quarter ($0.25)] and hold to hundredths place
    return (Math.round(price * 2) / 2).toFixed(2); 
};

/* ------------     Custom Definition Management   ------------ */ 
//Recieves array of objects (customField descriptors)
//Returns object with keys [field id] and values [customField info]
const getCustomDefinition = (array: any[]) => {
    let newObj = {};
    // @ts-ignore
    array.map((obj) => { newObj[obj.id] = new CustomField(obj); });
    return newObj;
}

//Accepts object representing Trello custom field
//Creates new object with a name field directing to values
const CustomField = function (this: any, field: any) {
    this.name = field.name;
    this.type = field.type;
    // Set options definitions if custom field type is a "list"
    switch (field.type) {
        case `list`:
            field.options.map((option: any) => 
                this[option.id] = {
                    value: option.value.text,
                    color: option.color
                });
            break;
        case `checkbox`: 
    }
}

export { getData};