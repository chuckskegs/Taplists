import * as functions from 'firebase-functions';
import express from 'express';
import { getData } from "./model";
import { SquareID } from './variables';
import { Client, Environment } from "square"; 
import crypto from 'crypto';
require('dotenv').config()
const app = express();

const client = new Client({
    accessToken: process.env.SQUARE_ACCESS_TOKEN,
    environment: Environment.Production
});

/**
 * Retreives trello data (allCards) from the proper Store then
 * retrieves the JSON data (response) for all 50 existing catalog items on Square
 * Sends both trello data and JSON data to batchUpdate Square
 */
async function updateSquare(shopName: string){
    let tapID: any[] = SquareID.CDTapID;
    if(shopName === 'GW') tapID = SquareID.GWTapID;
    else if(shopName === 'SP') tapID = SquareID.SPTapID;
    try {
        const allData = await Promise.all([getData(shopName)]);
        const allCards = allData.flatMap((res: any) => res);
        const response = await client.catalogApi.batchRetrieveCatalogObjects({
            objectIds: tapID
        });
        await batchUpdate(allCards, response.result.objects);
    } catch (error) {
        console.log(`${shopName} Error:`, error);
    }
}

/**
 * @allCards :Trello data that includes updated item metadataand prices
 * @squareData :JSON data with the old metadata for taplist items on POS 
 * Takes old JSON data from Square and updates with Trello data then uses
 * the updated JSON to make the batch call to update the Square taplists
 */
async function batchUpdate(allCards: any, squareData: any){
    let index = 0;
    let varNames: any[] = ["Full(16oz, Nitro, or 1L)", "Half(8oz or 1/2L)", "Quarter(4oz)", "Fill: 32oz", "Fill: 32oz + Crowler", "Fill: 64oz"];

    squareData.forEach((element : any )=> {
        let updatedItem = allCards[index++];
        element.itemData.name = "#" + updatedItem.tap + ": " + updatedItem.beer;
        console.log(element.itemData.name);
        for(let i = 0; i < 6; i++){
            element.itemData.variations[i].itemVariationData.name = varNames[i];
            element.itemData.variations[i].itemVariationData.sellable = true;
        }
        if(updatedItem.Full === 0) element.itemData.variations[0].itemVariationData.sellable = false;
        if(updatedItem.Half === 0) element.itemData.variations[1].itemVariationData.sellable = false;
        if(updatedItem.Quarter === 0) element.itemData.variations[2].itemVariationData.sellable = false;
        if(updatedItem.Crowler === 0) element.itemData.variations[4].itemVariationData.sellable = false;
        if(updatedItem.Growler === 0){
            element.itemData.variations[3].itemVariationData.sellable = false;
            element.itemData.variations[5].itemVariationData.sellable = false;
        }
        element.itemData.variations[0].itemVariationData.priceMoney.amount = updatedItem.Full * 100;
        element.itemData.variations[1].itemVariationData.priceMoney.amount = updatedItem.Half * 100;
        element.itemData.variations[2].itemVariationData.priceMoney.amount = updatedItem.Quarter * 100;
        element.itemData.variations[3].itemVariationData.priceMoney.amount = (updatedItem.Growler * 100) / 2;
        element.itemData.variations[4].itemVariationData.priceMoney.amount = updatedItem.Crowler * 100;
        element.itemData.variations[5].itemVariationData.priceMoney.amount = updatedItem.Growler * 100;
    });
    try {
        await client.catalogApi.batchUpsertCatalogObjects({
            idempotencyKey: crypto.randomUUID(),
            batches: [{ 
                objects: squareData
            }]
        });
        console.log("Shop updated " + allCards[0].shop);
    } catch(error) {
        console.log(error);
    }
}

/**
 * Manages data requests: *./app/data
 * Uses query info from request to determine which shop data to get
 */
app.all('/data', (request: express.Request, response: express.Response) => {
    let menu = request.query.menu;
    if (request.method === "GET" && !request.query) { 
        menu = "CD2";
    }
    //@ts-ignore
    getData(menu).then((cards) => {
        response.json(cards);
    }).catch((err) => 
        response.send(`Error: ${err}`)
    );
});

// Express endpoint to run full update when recieves request
//FIXME: send a proper response.
app.all('/updateSquare', async (request: express.Request, response: express.Response) => {
    await updateSquare('CD');
    await updateSquare('SP');
    await updateSquare('GW');
    response.send("Updating shops to SQUARE.");
});

exports.app = functions.https.onRequest(app);